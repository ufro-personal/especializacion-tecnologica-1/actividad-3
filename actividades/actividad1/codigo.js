var send = document.getElementById("btn")

var espacios = Boolean;

var aleatorio = document.getElementById("Random")
var err = document.getElementById("error")

//obtenemos n1 como minimo y n2 como maximo a traves del elemento input
var inputUno = document.getElementById("input-1");
var inputDos = document.getElementById("input-2");

//funcion validar campos vacios
function camposVacios(n1, n2) {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    // si n1 y n2 no estan vacios retorna true
    if (n1 != "" && n2 != "") {
        return true;
    } else {
        //si los campos estan vacios retorna false
        aleatorio.innerHTML = ("");
        err.innerHTML = ("entradas vacias");
        return false;
    }
}
//damos la funcion click al boton send
send.addEventListener("click", function() {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    err.innerHTML = ("");
    aleatorio.innerHTML = generarRandom(n1, n2);
});

function generarRandom(n1, n2) {
    espacios = camposVacios(n1, n2);
    if (espacios == true) {
        return Math.round((Math.random() * (n2 - n1) + parseInt(n1)));
    } else {
        return aleatorio.innerHTML = ("")
    }
}